#!/bin/bash

# Installer script for qq2clone

#------------------------------------------------------------------------#
# Copyright 2021, Jesse Gardner                                          #
#------------------------------------------------------------------------#
# This file is part of qq2clone.                                         #
#                                                                        #
# qq2clone is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 2 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# qq2clone is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with qq2clone.  If not, see <https://www.gnu.org/licenses/>.     #
#------------------------------------------------------------------------#

# base64 encoded tgz archive will be added by gen_all.bash in archive below

#REPLACE WITH ARCHIVE#

[[ -z "$HOME" ]] &&
  { echo '$HOME must be defined'; exit 1; }

temp_dir="$(mktemp -d)" ||
  { echo "Failed to create temp directory"; exit 1; }
trap "rm -rf \"${temp_dir:?}\" &>/dev/null" EXIT

{
  cd "$temp_dir" &&
  echo "$archive" | base64 -d > archive.tgz &&
  tar -xzf archive.tgz &&
  rm archive.tgz
} || { echo "Problem unpacking data to $temp_dir"; exit 1; }

declare input="" location="${HOME}/storage-qq2clone"
while [[ ! "$input" =~ ^(y|n|Y|N)$ ]]; do
  echo "[y/n] Use default storage directory '$location' ?"
  read -rn1 input; echo
done

while [[ ! "$input" =~ (y|Y) ]]; do
  echo "Type in new location"
  read -r location; input=""
  while [[ ! "$input" =~ ^(y|n|Y|N)$ ]]; do
    echo "[y/n] Accept location '$location' ?"
    read -rn 1 input; echo
  done
done
echo

{
  mkdir -p "${HOME}/.config" &&
  echo "$location" > "${HOME}/.config/qq2clone"
} || { echo "Failed to write to ${HOME}/.config/qq2clone"; exit 1; }

{
  mkdir -p "$location" &&
  find -mindepth 1 -exec mv \{\} "$location"/\{\} \;
} || { echo "Cannot create/write to '$location'"; exit 1; }

if { command -v sudo &>/dev/null || [[ "$USER" == "root" ]]; }; then

  input=""
  qq_moved=0
  while : ; do
    while [[ ! "$input" =~ ^(y|n|Y|N)$ ]]; do
      echo "[y/n] Move main script file to /usr/bin/qq2clone ?"
      read -rn 1 input; echo
    done
    if [[ "$input" =~ ^(y|Y)$ ]]; then
      success=1
      if [[ "$USER" == "root" ]]; then
        mv "${location}/qq2clone" /usr/bin/qq2clone || success=0
      else
        sudo mv "${location}/qq2clone" /usr/bin/qq2clone || success=0
      fi
      ((success)) && { echo "File moved"; qq_moved=1; break; }
      echo "Attempt failed."; input=""; continue
    else
      echo "You have chosen to leave the main script file at:"
      echo "  ${location}/qq2clone"
      break
    fi
  done
  echo

  input=""
  while : ; do
    while [[ ! "$input" =~ ^(y|n|Y|N)$ ]]; do
      echo "[y/n] Move man page to /usr/local/man/man1/qq2clone.1 ?"
      read -rn 1 input; echo
    done
    if [[ "$input" =~ ^(y|Y)$ ]]; then
      success=1
      if [[ "$USER" == "root" ]]; then
        {
          mkdir -p /usr/local/man/man1 &&
          mv "${location}/qq2clone.1" /usr/local/man/man1/qq2clone.1
        } ||
          success=0
      else
        {
          sudo mkdir -p /usr/local/man/man1 &&
          sudo mv "${location}/qq2clone.1" /usr/local/man/man1/qq2clone.1
        } ||
          success=0
      fi
      ((success)) && { echo "File moved"; break; }
      echo "Attempt failed."; input=""; continue
    else
      echo "You have chosen to leave the man page at:"
      echo "  ${location}/qq2clone.1"
      break
    fi
  done

else
  echo "sudo is not installed on this system, so script and man page files"
  echo "will not be moved to /usr/bin/ and /usr/local/man"
  echo "  [Main script] ${location}/qq2clone"
  echo "  [Man page]    ${location}/qq2clone.1"
  echo "Manually move these files to desired location on this system"
fi
echo

input=""
while [[ ! "$input" =~ ^(y|n|Y|N)$ ]]; do
  echo "[y/n] qq2clone has a bash completion script. Modify ~/.bashrc to"
  echo "      source the completion script?"
  read -rn 1 input; echo
done

if [[ "$input" =~ ^(y|Y)$ ]]; then
  if echo "source ${location}/qq2clone_completion.bash" >> \
       "${HOME}/.bashrc"; then
    echo ".bashrc modified."
  else
    echo "Failed to write to ${HOME}/.bashrc . Completion script is"
    echo "available at:"
    echo "  ${location}/qq2clone_completion.bash"
  fi
else
  echo "Completion script is available at:"
  echo "  ${location}/qq2clone_completion.bash"
fi
echo

if ((qq_moved)); then
  /usr/bin/qq2clone setup
else
  "${location}/qq2clone" setup
fi
setup_code=$?

if ((setup_code != 0)); then
  echo "Setup failed. Resolve problem(s) described, then manually run"
  echo "the setup again using: qq2clone setup"
fi

exit $setup_code

#!/bin/bash

#------------------------------------------------------------------------#
# Copyright 2021, Jesse Gardner                                          #
#------------------------------------------------------------------------#
# This file is part of qq2clone.                                         #
#                                                                        #
# qq2clone is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 2 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# qq2clone is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with qq2clone.  If not, see <https://www.gnu.org/licenses/>.     #
#------------------------------------------------------------------------#

# Bash completion for qq2clone

_qq2clone () {

[[ -n "$QQ2_DIR" ]] || QQ2_DIR="$(<"${HOME:?}/.config/qq2clone")"
[[ -e "${QQ2_DIR}/qq2clone.db" ]] || return 1

declare -a templates
declare line
while read -r line; do
  templates=( "${templates[@]}" "$line" )
done < <("sqlite3" --batch "${QQ2_DIR}/qq2clone.db" \
  "select name from TEMPLATES")
declare -a COMS FLAGS
COMS=( check clone config connect copy-template delete-template destroy \
  edit exec import-template list list-templates modify-template restore \
  resume rm rm-shred rm-wipe save save-rm start suspend )
FLAGS=( connection copy-disks  no-spice use-spice help no-run quiet quieter  \
  run spicy storage template verbose virt-viewer )

local LAST_ARG THIS_ARG B4_LAST_ARG P set_coms
(( COMP_CWORD > 0 )) &&
  LAST_ARG="${COMP_WORDS[$((COMP_CWORD - 1))]}"
(( COMP_CWORD > 1 )) &&
  B4_LAST_ARG="${COMP_WORDS[$((COMP_CWORD - 2))]}"
THIS_ARG="${COMP_WORDS[$COMP_CWORD]}"
set_coms="connect|destroy|exec|resume|rm|rm\-shred|rm\-wipe|save|save\-rm|"
set_coms="${set_coms}start|suspend|restore"

[[ "$THIS_ARG" =~ ^('./'|'/') ]] &&
  { read -ra COMPREPLY < <(compgen -f "$THIS_ARG" | tr "\n" " "  ); return 0; }

declare -a suggestions

if [[ "$THIS_ARG" =~ ^\-\- ]]; then
  suggestions=("${FLAGS[@]}")
  P="--"
elif [[ "$LAST_ARG" =~ ^(\-|modify|delete|copy)\-template$ ]] ||
  [[ "$LAST_ARG" == "template-snapshot" ]] ||
  [[ "$LAST_ARG" == "-t"  ]]; then
  suggestions=( "${templates[@]}" )
elif [[ "$LAST_ARG" =~ ^(\-\-storage|\-s)$ ]]; then
  read -ra suggestions < <(virsh pool-list --name | tr -d '\n')
elif [[ "$LAST_ARG" == "config" ]]; then
  suggestions=( list edit info )
elif [[ "$LAST_ARG" =~ ^(${set_coms})$ ]]; then
  suggestions=( all running saved off in-shutdown idle paused crashed )
  suggestions=( "${suggestions[@]}" pmsuspended )
elif [[ "$LAST_ARG" == "list" ]] &&
  [[ ! "$B4_LAST_ARG" == "config" ]]; then
  suggestions=( all xml )
else
  local curr_com word elem
  for word in "${COMP_WORDS[@]}"; do
    for elem in "${COMS[@]}"; do
      [[ "$elem" == "$word" ]] && { curr_com="$word"; break 2; }
    done
  done
  
  if [[ -n "$curr_com" ]]; then
    if [[ "$curr_com" == "modify-template" ]] &&
      [[ "${COMP_WORDS[$((COMP_CWORD - 2))]}" == "$curr_com"  ]]; then
      suggestions=( edit prepare-image commit-image discard-image \
        destroy-image rename )
    elif [[ "$curr_com" == config ]] &&
      [[ "${COMP_WORDS[$((COMP_CWORD - 2))]}" == "$curr_com"  ]] &&
      [[ "$LAST_ARG" =~ ^(info|edit)$ ]]; then
      suggestions=(NORUN QUIET USE_SPICE SPICY STORAGE S_TIMEOUT TEMPLATE \
        TEMPLATE_DIR)
    else
     suggestions=(  )
    fi
  else
    suggestions=("${COMS[@]}")
  fi
fi

local i
declare -a comp
for ((i=0;i<${#suggestions[@]};i++)); do
  if [[ "${P}${suggestions["$i"]}" =~ ^"${THIS_ARG}" ]]; then
    comp=( "${comp[@]}" "${suggestions["$i"]}" )
  fi
done

(( ${#comp} > 0 )) &&

read -ra COMPREPLY < <(compgen -P "${P}" -W "${comp[*]}" | tr "\n" " ")
}

complete -F _qq2clone qq2clone

#!/bin/bash

# Produce documentation from pandoc file, compile lv_api_do, and add tgz
# archives into the installer and main script

#------------------------------------------------------------------------#
# Copyright 2021, Jesse Gardner                                          #
#------------------------------------------------------------------------#
# This file is part of qq2clone.                                         #
#                                                                        #
# qq2clone is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 2 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# qq2clone is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with qq2clone.  If not, see <https://www.gnu.org/licenses/>.     #
#------------------------------------------------------------------------#


declare -a req_files=( qq2clone_noarchive man.pandoc
qq2clone_completion.bash  qq2clone_installer_noarchive.bash "lv_api_do.c"
man.pandoc LICENSE)

# Check for required files

has_req=1
for file in "${req_files[@]}"; do
  if [[ ! -e "$file" ]]; then
    has_req=0
    echo "Current working directory missing file '$file'"
  fi
done
((has_req)) || exit 1

# Generate manual from pandoc source in two formats

echo "Generating manual"
pandoc -s -f markdown -t markdown_strict -o man.md man.pandoc
pandoc -s -f markdown -t man -o qq2clone.1 man.pandoc

# Compile lv_api_do

echo "Compiling lv_api_do"
gcc lv_api_do.c -o lv_api_do -lvirt

# Generate qq2clone
echo "Generating qq2clone"
while IFS= read -r line; do
  if [[ "$line" == "#REPLACE WITH ARCHIVE#" ]]; then
    echo "archive='"
    tar -cz LICENSE lv_api_do | base64
    echo "'"
  else
    echo "$line"
  fi
done < qq2clone_noarchive > qq2clone
chmod +x qq2clone

# Generate installer
echo "Generating installer script"
while IFS= read -r line; do
  if [[ "$line" == "#REPLACE WITH ARCHIVE#" ]]; then
    echo "archive='"
    tar -cz qq2clone qq2clone.1 qq2clone_completion.bash | base64
    echo "'"
  else
    echo "$line"
  fi
done < qq2clone_installer_noarchive.bash > qq2clone_installer.bash
chmod +x qq2clone_installer.bash


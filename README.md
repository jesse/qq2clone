# README

qq2clone is a linux tool to simplify creating and managing copy-on-write clones of template QEMU/KVM virtual machines. It can perform batch operations in case you are working with multiple clones. There is a version of the manual page you can read online above ([man.md](https://git.j-g-web.com/jesse/qq2clone/src/branch/master/man.md)).

qq2clone has these dependences:
*	virsh (qq2clone has started transitioning to use the libvirt API directly, this will not remain a dependency forever)
*	Bash 4.0+
*	sqlite3
*	xmllint (from libxml2)
*	qemu\-img
*	libvirt tools:
	*	virt\-clone
	*	virt\-xml
	*	virt\-xml\-validate

# INSTALL

The file [qq2clone_installer.bash](https://git.j-g-web.com/jesse/qq2clone/raw/branch/master/qq2clone_installer.bash) is a setup script containing a self-extracting archive. If you copy and paste the following into a terminal, it will download and run.
```
curl  https://git.j-g-web.com/jesse/qq2clone/raw/branch/master/qq2clone_installer.bash > .qq2clone_installer.bash 2> /dev/null &&
chmod +x .qq2clone_installer.bash &&
./.qq2clone_installer.bash
```

# FAQ


## How do I use this thing?

The
[manual](https://git.j-g-web.com/jesse/qq2clone/src/branch/master/man.md)
is quite detailed, and there is an
[examples](https://git.j-g-web.com/jesse/qq2clone/src/branch/master/man.md#examples) section that may be helpful.

## Why use Bash and C when this could all be written in Python?

Just to improve my Bash skills and gain a tiny bit of familiarity with C.
Writing this in Python would certainly have been easier.

## Permission issues when making a clone

Getting an error like "Template $VMNAME refers
to /var/lib/libvirt/images/$VMNAME.qcow2, but the file
cannot be read" ? This is because libvirt is configured with a default
storage pool that you, as a non-root user are not allowed to read.

You can resolve this issue by moving the image associated with a
virtual machine to a location your user can read, changing the
ownership/permissions, and modifying the libvirt xml of the
template to point at the new image location. To modify a template machine's
XML, use qq2clone modify\-template VMNAME edit. You will have to
find the filepath of the virtual machine image in the XML and change it
to point at the location you moved it to.

# Copyright 2021, Jesse Gardner

All files present in the qq2clone git repository are released under the
the GNU GPL v2 license. The following disclaimer is found in most files
contained in this repository, and makes things more specific:
```
#------------------------------------------------------------------------#
# Copyright 2021, Jesse Gardner                                          #
#------------------------------------------------------------------------#
# This file is part of qq2clone.                                         #
#                                                                        #
# qq2clone is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 2 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# qq2clone is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with qq2clone.  If not, see <https://www.gnu.org/licenses/>.     #
#------------------------------------------------------------------------#
```
